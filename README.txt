Package Field Module
=====================

This module provides a field type of package for shipping integration with
Drupal Commerce

It currently does these things:

1. Provides a field type to store shipping package information on Drupal
   Commerce products.
2. The available fields are:

  Shippable - Is the product shippable
  Weight Unit -How is the product weighed (ie: pounds, kilograms, etc...)
  Weight -Weight of the product
  Size Unit -How is the product measured (ie: inches, millimeters)
  Length -Length of the product package
  Width -Width of the product package
  Height -Height of the product package
  Quantity -How many products can fit into the package described above

To configure:
-------------

1. Install and enable the module.
2. Manually add this as a field to your product type or download and install
   one of the modules that use it.
